/*-
 * Copyright 2021 Alexander Mikhaylenko <alexm@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor
 * Boston, MA 02110-1335 USA.
 */

[GtkTemplate (ui = "/org/gnome/gitlab/exalm/Impatience/window.ui")]
public class Impatience.Window : Gtk.ApplicationWindow {
    [GtkChild]
    private unowned Gtk.Stack stack;

    public ColorScheme color_scheme { get; set; }

    private Backend backend;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        backend = new Backend ();
        backend.bind_property ("color-scheme", this,
                               "color-scheme", BIDIRECTIONAL | SYNC_CREATE);

        maybe_show_error ();

        backend.notify["connected"].connect (() => {
            maybe_show_error ();
        });
    }

    private void maybe_show_error () {
        if (!backend.connected)
            stack.visible_child_name = "error";
    }

    static construct {
        install_action ("win.set_color_scheme", "i", (widget, action_name, param) => {
            var self = widget as Window;
            self.color_scheme = (ColorScheme) param.get_int32 ();
        });
    }

    [GtkCallback]
    private bool is_equal (ColorScheme scheme1, ColorScheme scheme2) {
        return scheme1 == scheme2;
    }

    [GtkCallback]
    private string get_button_icon_name (string orig, bool dark) {
        if (dark) {
            if (orig == "light")
                return "dark-symbolic";
            if (orig == "dark")
                return "light-symbolic";

            return "dark-light-symbolic";
        }

        return @"$orig-symbolic";
    }
}
