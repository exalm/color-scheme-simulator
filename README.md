# Color Scheme Simulator

Provides color schemes on systems with GNOME 40 or 41. It can be useful for
testing apps when working on the [dark style preference initiative][0].

![Color scheme Simulator, default colors](screenshots/screenshot-default.png)
![Color scheme Simulator, dark colors](screenshots/screenshot-dark.png)

Color scheme simulator consists of two parts: an [xdg-desktop-portal][1]
implementation providing a settings portal that provides the standard
`org.freedesktop.appearance.color-scheme` key, and a GUI for toggling the color
scheme.

## Portal

The portal code is partially based on the [elementary settings portal][2].

It has to run on the host system and be installed into `/usr`.

Building:

```shell
meson build --prefix=/usr -Dgui=false
cd build
ninja
sudo ninja install
systemctl --user restart xdg-desktop-portal
```

Uninstalling:

```shell
sudo ninja uninstall
systemctl --user restart xdg-desktop-portal
killall xdg-desktop-portal-impatience
```

It only depends on gio and systemd.

## GUI

When the portal is running, the GUI can be used from Flatpak without problems.
Just building it in GNOME Builder should be enough.

[0]: https://gitlab.gnome.org/GNOME/Initiatives/-/issues/32
[1]: https://github.com/flatpak/xdg-desktop-portal/
[2]: https://github.com/elementary/settings-daemon/pull/38
